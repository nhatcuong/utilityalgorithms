import math;

import numpy.random;

__author__ = 'nhatcuong'


def selection_sort(A):
    """
    O(n2). useful compared to the insertion sort when
    1. the swap is typically consuming. selection_sort only move elements O(n) times in worst case.
    2. the array is not "already almost sorted". When it is the case, insertion_sort takes over coz it becomes O(n)
    """
    for i in range(0, len(A)):
        smallest = i
        for j in range(i + 1, len(A)):
            if A[j] < A[smallest]:
                smallest = j
        swap(A, i, smallest)
    return A


def insertion_sort(A):
    """
    O(n2) in worst case. useful when the array is already almost sorted,
    where the original position of each element is not much far from where it will be sorted
    """
    for i in range(0, len(A) - 1):
        j = i + 1
        while j > 0 and A[j - 1] > A[j]:
            swap(A, j, j - 1)
            j -= 1
    return A


def merge_sort(A):
    """
    O(nlogn) but the constant is relatively high compared to selection and insertion sorts
    So if you only have to sort a small number of elements, there s might be no
    """
    begin = 0
    end = len(A) - 1
    output = [0] * len(A)
    merge_sort_from_begin_to_end(A, begin, end)
    return A


def merge_sort_from_begin_to_end(A, begin, end):
    assert end >= begin
    if end == begin:
        return
    if end - begin == 1:
        if A[begin] > A[end]:
            swap(A, begin, end)
        return
    mid = int(round(math.floor((end + begin) / 2)))
    merge_sort_from_begin_to_end(A, begin, mid)
    merge_sort_from_begin_to_end(A, mid + 1, end)
    left_index = begin
    right_index = mid + 1
    current_index = 0
    output = [0] * (end - begin + 1)
    while current_index < len(output):
        if left_index <= mid and right_index <= end:
            if A[right_index] < A[left_index]:
                output[current_index] = A[right_index]
                right_index += 1
            else:
                output[current_index] = A[left_index]
                left_index += 1

        elif left_index > mid:
            output[current_index] = A[right_index]
            right_index += 1
        elif right_index > end:
            output[current_index] = A[left_index]
            left_index += 1
        current_index += 1
    for i in range(0, len(output)):
        A[begin + i] = output[i]


def swap(A, i, j):
    assert i < len(A) and j < len(A)
    medium = A[i]
    A[i] = A[j]
    A[j] = medium


A = [int(1000 * numpy.random.random()) for i in xrange(10)]
print(A)
print merge_sort(A)
