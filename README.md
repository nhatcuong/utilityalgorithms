# README #

I find that basic algorithms, like sorting, can be unintuitive and hard to master.
So I am relearning them and try to implement them in Python, a language that I'm learning too.

In each file there might be one or multiple algos to solve the same problems.

Test cases will be at the end of the file.

Normally you will just have to install python and all the codes are ready to run.